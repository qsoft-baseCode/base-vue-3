module.exports = {
	chainWebpack: config => {
		// 路径别名
		const src = config.resolve.alias.get('@')
		config.resolve.alias.set('@fun', src + '/service/sometime')
	},
	productionSourceMap: false, // 不需要生产环境的 source map
	// baseUrl: '',
}
