const cn2al = s => {
	if (typeof s !== 'string') {
		return 0
	}
	const ha = {
		一: 1,
		二: 2,
		两: 2,
		三: 3,
		四: 4,
		五: 5,
		六: 6,
		七: 7,
		八: 8,
		九: 9,
		零: 0,
	}
	const hb = {
		十: 10,
		百: 100,
		千: 1000,
	}
	const hc = {
		万: 10000,
		亿: 100000000,
		兆: 1000000000000,
	}
	let k = 1
	let k4 = 1
	let num = 0
	let king = true
	for (let i = s.length - 1; i > -1; i--) {
		const a = s[i]
		if (hc[a] !== undefined) {
			k4 = hc[a]
			k = 1
		}
		if (hb[a] !== undefined) {
			k = hb[a]
			king = true
		}
		if (ha[a] !== undefined) {
			num += ha[a] * k * k4
			king = false
		}
	}
	if (king) {
		num += 1 * k * k4
	}
	return num
}
const al2cn = n => {
	n = String(n)
	const len = n.length
	let ha = ['零', '一', '二', '三', '四', '五', '六', '七', '八', '九', '十']
	const hb = ['', '十', '百', '千']
	const hc = ['', '万', '亿', '兆']
	if (n <= 10) {
		return ha[n]
	}
	if (n < 20) {
		return '十' + ha[n - 10]
	}
	let re = ''
	let has0 = false
	let begin0 = false
	for (let i = 0; i < len; i++) {
		const a = n[len - i - 1]
		if (i % 4 === 0) {
			begin0 = false
			if (has0) {
				re = '零' + re
				has0 = false
			}
			re = hc.shift() + re
		}
		if (a === '0') {
			if (begin0) {
				has0 = true
			}
		} else {
			begin0 = true
			if (has0) {
				re = '零' + re
				has0 = false
			}
			re = ha[a] + hb[i % 4] + re
		}
	}
	return re.replace(/^一十/, '十')
}
export {al2cn, cn2al}
/* 
做中国数字关键是
1. 每4位一轮, 切换万亿兆, 每轮都用千百十
2. 零的省略
3. 一十开头一的省略

*/
