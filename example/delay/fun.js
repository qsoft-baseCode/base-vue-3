const do_wait = {
	can: true,
	timer: null,
	init(fun) {
		return n => {
			if (!this.can) {
				return
			}
			fun(n)
			this.can = false
			this.timer = setTimeout(() => {
				this.can = true
			}, 3000)
		}
	},
}
const wait_do = {
	timer: null,
	init(fun) {
		return n => {
			clearTimeout(this.timer)
			this.timer = setTimeout(() => {
				fun(n)
			}, 2000)
		}
	},
}
export {do_wait, wait_do}
