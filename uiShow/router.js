export default {
	path: '/ui',
	name: 'ui',
	component: () => import('./a'),
	children: [
		{path: 'button', alias: '', component: () => import('./button/index')},
		{path: 'toast', component: () => import('./toast/index')},
		{path: 'dialog', component: () => import('./dialog/index')},
		{path: 'drag', component: () => import('./drag/index')},
	],
}
// alias 别名, 此时/example 和 /example/if展示同样内容
