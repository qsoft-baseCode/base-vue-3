## 使用方法

<qiu-btn />

## 参数

| 必要参数 |        |                                |
| -------- | ------ | ------------------------------ |
| txt      | string | 按钮上显示的文字               |
| @clk     | func   | 点击事件,回调参数 back(string) |

| 可选参数           |         |                                              |
| ------------------ | ------- | -------------------------------------------- |
| back               | string  | 点击事件的回调参数, 可以用来区分按钮         |
| sty                | string  | 使用某个默认样式组                           |
| fetching           | boolean | 是否处于网络请求状态, true 时不可点击        |
| time               | number  | 倒计时需要时间                               |
| beforeimg/afterimg | styring | 前置/后置图片的地址或 base64                 |
| instyle            | object  | box, txt, beforeimg, afterimg 来设定内联样式 |
| noevent            | boolean | true 时按钮不会绑定事件                      |

## 样式

直接给组件添加 class 也可以改变样式
支持传入绑定那样样式

## 结构

```
<button>
    <img>
    <span />
    <img>
</button>
```

## 关于 time

设定了 time, 同时按钮有文字显示时, 点击按钮就会进入倒计时数字状态, 鼠标手势改变

## dataset

传入的 back 同时会设定 data-back, 配合 noevent 得到干净无事件按钮, 方便使用事件委托
