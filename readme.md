## 启动及打包

| 行为 | 命令                        |
| ---- | --------------------------- |
| 启动 | yarn serve 或 npm run serve |
| 打包 | yarn build 或 npm run build |

## 项目结构

-   其他
-   .prettierrc // .prettier 插件格式化配置
-   example // 一些 vue 示例和预置函数
-   uiShow // 展示预置 ui 组件的用法
-   qsoftui // 预置 ui 组件
-   src
    -   service // 函数
        -   aways // 非常常用的, 直接挂到 vue 原型链上
        -   sometime // 偶尔用, 按需引入
    -   store // vuex 状态管理
    -   router.js // 路由管理
    -   style // 通用样式文件
    -   view // 视图层
    -   component // 组件
    -   其他

## 规范

### 命名

| 文件及文件夹 |        |
| ------------ | ------ |
| 文件夹       | 小驼峰 |
| .vue         | 大驼峰 |
| .js/css/md   | 小驼峰 |

| .vue 文件内 |                                                    | 属性顺序                                                           |
| ----------- | -------------------------------------------------- | ------------------------------------------------------------------ |
| template    | 遵循 html 规范, 全小写, 双引号                     | [for,if],[id,calss],[event],[html-artus,user-artus])               |
| script      | 应用 prettier 规则, 小驼峰, 无分号, 单引号, 4 空格 | [name],[components],[props,data,computed],[生命周期,watch,methods] |
| style       | 遵循 css 规范, 全小写加短横杠                      |                                                                    |

### 其他

-   script
    -   方法涉及网络请求, 前缀 api\_
    -   方法由可以用户触发, 前缀 e\_
    -   方法跳转路由或销毁实例, 后缀\_
    -   组件命令最好是短横杠连双单词, 如 c-list, 以预防 html 将来增加 list 标签
-   css 属性书写顺序, 从外到内, 从大到小
    1. display
    2. position
    3. z-index, [top, bottom], [left, right]
    4. margin
    5. over-flow
    6. box-sizing(此条决定 下两条是否颠倒顺序)
    7. border, padding
    8. width, height
    9. bg-color, animation, transition, curser...
    10. \[align-tiems, text-align](内部元素如何排列)
    11. line-height
    12. font-size
    13. color
