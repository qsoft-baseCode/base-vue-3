import Vue from 'vue'
import VueToast from './toast.vue'

const Compo = Vue.extend(VueToast)
const instance = new Compo().$mount()
document.body.appendChild(instance.$el)

const ToastStart = (param = {}) => {
	/*
        传入: 可以传入一段字符串或一个对象{文字, 持续时间}
        返回: 无
    */
	if (typeof param === 'string') {
		instance.append(param)
		return
	}
	// 切换时间
	let to = param.timeout || 2000
	if (to < 10) {
		to *= 1000
	}
	to = Math.max(2000, to)
	instance.timeout = to
	instance.append(param.msg)
}

const install = Vue => {
	Vue.prototype.$toast = ToastStart
}

export default install
