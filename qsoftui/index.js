import vue from 'vue'

import button from './button.vue'
vue.component('qiu-btn', button)
import dialog from './dialog.vue'
vue.component('qiu-dialog', dialog)
import drag from './drag.vue'
vue.component('qiu-drag', drag)
