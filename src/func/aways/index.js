import Vue from 'vue'
import fetch from './fetch' // 网络请求
import time from './time'
import {getOs} from './hardware'
import {mouseDrag} from './mouse'
import notify from './notify'
// array是特殊方法, 直接修改了array原型链
// import {arr} from './array'
// arr.add('max', 'min', 'remove')
Vue.prototype.$func = {
	fetch,
	time,
	getOs,
	mouseDrag,
	notify,
}
