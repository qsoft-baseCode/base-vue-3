# 常用函数

具体用法请看 EXAMPLE.js 或对应文件内注释

[网络请求](#axios)
[数组](#array)
[硬件及环境](#hardware)
[时间](#time)
[鼠标](#mouse)
[通知](#notify)

## axios

进行网络请求, 只有 post 和 get, 暂时没有上传图片

## array

修改数组原型链

-   最小值
-   最大值
-   删除

## hardware

环境相关

-   获取机器是 mac 还是 windows
-   获取浏览器

## time

时间相关

-   根据时间戳获取时间

## mouse

鼠标相关

-   拖拽的长度, 方向等

## notify

通知相关
