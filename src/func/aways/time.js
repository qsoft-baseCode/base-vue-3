function full(s) {
	return String(s).padStart(2, '0')
}
function Time(a) {
	const t = a ? new Date(a.toString()) : new Date()
	const Y = full(t.getFullYear())
	const M = full(t.getMonth() + 1)
	const D = full(t.getDate())
	const h = full(t.getHours())
	const m = full(t.getMinutes())
	const s = full(t.getSeconds())
	const re = {
		int: t.getTime(),
		Y,
		M,
		D,
		h,
		m,
		s,
		format(b0, b1) {
			if (b0 === undefined) {
				const a = `${Y}年${M}月${D}日`
				const b = `${h}时${m}分${s}秒`
				return a + ' ' + b
			} else {
				b1 = b1 || b0
				const a = [Y, M, D]
				const b = [h, m, s]
				return a.join(b0) + ' ' + b.join(b1)
			}
		},
	}
	return re
}

export default Time
