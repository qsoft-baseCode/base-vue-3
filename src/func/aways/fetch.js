import axios from 'axios'
const qs = require('qs')

const urls = {
	local: 'http://localhost:8080/',
	test: 'http://192.168.0.138:8000/FH-WEB2/',
	onLine: 'http://www.qiuwuyi.com/',
}

const ax0 = axios.create({
	baseURL: urls.test,
	headers: {
		// 'Content-Type': 'application/x-www-form-urlencoded;charset=UTF-8',
	},
	method: 'POST',
	// withCredentials: true,  让后台可以使用session
})

// 设置过滤器
ax0.interceptors.request.use(
	function(config) {
		if (localStorage.token) {
			config.headers.Authorization = localStorage.token
		}
		return config
	},
	function(error) {
		return Promise.reject(error)
	},
)
ax0.interceptors.response.use(
	function(response) {
		const re = response.data
		return re
	},
	function(e) {
		return {code: 0}
	},
)
// 封装使用时统一
const fetch = {
	post(link, params) {
		const s = qs.stringify(params)
		return ax0.post(link, s)
	},
	get(link, params) {
		const s = {params}
		return ax0.get(link, s)
	},
}

export default fetch
