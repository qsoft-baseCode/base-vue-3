const notifyMe = txt => {
	// 先检查浏览器是否支持
	if (!('Notification' in window)) {
		alert(txt)
	} else if (Notification.permission === 'granted') {
		// 检查用户是否同意接受通知
		return new Notification(txt)
	} else if (Notification.permission !== 'denied') {
		// 否则我们需要向用户获取权限
		Notification.requestPermission(permission => {
			// 如果用户同意，就可以向他们发送通知
			if (permission === 'granted') {
				return new Notification(txt)
			}
		})
	}
	// 最后，如果执行到这里，说明用户已经拒绝对相关通知进行授权
	// 出于尊重，我们不应该再打扰他们了
}
// 桌面通知
export default notifyMe
