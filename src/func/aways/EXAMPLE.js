// 除了改变 js 原型链的方法, 都被挂到$fun命名空间下, 如this.$fun.post('src', params)

// ------------------

// array
// 增加了 array 原型链方法, 所以较为谨慎的按需添加
import {arr} from './array' // 在本目录 index.js, 不需要再次引入
arr.add('max', 'min', 'remove') // 添加需要的方法

// max, min
// 基本用法
const a = [2, 3, 4]
a.max // 4
a.min // 2
// 设置时尝试改变数组, 如果超过了原本的极值, 会添加到数组末尾
a.max = 6
a // [2, 3, 4, 6]
a.max = 5
a // [2, 3, 4, 6]
a.min = 1
a // [1, 2, 3, 4, 6]

// ------------------

// axios
// params 写成如此对象形式即可, 已封装好参数处理
const apis = async () => {
	const params = {
		name: '秋无衣',
		age: 2333,
	}
	const re0 = await this.$fun.post('src', params)
	const re1 = await this.$fun.get('src', params)
}

// ------------------

// time
// 初始化, 不传入时间戳时, 得到当前时间
const t0 = this.$fun.time.of()
// 或
const t1 = this.$fun.time.of(1540693220661)
t0.int // 1540693220661 时间戳
t0.Y // 四位数字年
// Y,M,D,h,m,s 年月日时分秒
t0.format0('-', ':') // 2018-10-28 10:20:20 可以传入不同的连接符
t0.format1() // 2018 年 10 月 28 日 10 时 20 分 20 秒

// ------------------

// hardware

const os = this.$fun.getOs() // win mac 如果判断不出则返回 false
