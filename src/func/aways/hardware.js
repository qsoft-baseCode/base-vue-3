const getOs = () => {
	if (navigator.platform.indexOf('Win') > -1) {
		return 'win'
	} else if (navigator.platform.indexOf('Mac') > -1) {
		return 'mac'
	} else {
		return false
	}
}
export {getOs}
// 判断机器是windows还是mac

const getBrowser = () => {
	// 判断浏览器, 不建议使用, 需要某个功能时单独检测功能兼容性是合适的做法
	const ua = window.navigator.userAgent.toString()
	// console.log(window.navigator.plugins) // 安装的插件
	if (ua.includes('Firefox')) {
		return 'Firefox'
	}
	if (ua.includes('Chrome')) {
		if (ua.includes('WOW')) {
			return ua.includes('NET') ? '360兼容' : '360急速'
		}
		return 'Chrome'
	}
	if (ua.includes('NET') && ua.includes('rv')) {
		return 'IE'
	}
}
