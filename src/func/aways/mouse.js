const mouseDrag = (e, lmt = 200, fun) => {
	if (e.type !== 'mousedown' || e.which !== 1) {
		return // 按下左键
	}
	const [x0, y0] = [e.pageX, e.pageY]
	const move = e => {
		const [x1, y1] = [e.pageX, e.pageY]
		const [lx, ly] = [Math.abs(x1 - x0), Math.abs(y1 - y0)]
		const re = {
			w: y0 - lmt > y1,
			s: y0 + lmt < y1,
			a: x0 - lmt > x1,
			d: x0 + lmt < x1,
			horizontal: lx > ly,
			length: Math.max(lx, ly),
			length2: Math.pow(lx * lx + ly * ly, 0.5) | 0,
		}
		fun(re)
		document.removeEventListener('mouseup', move)
	}
	document.addEventListener('mouseup', move)
}
export {mouseDrag}
/*
mouseDrag
params:
    e: 鼠标按下事件
    lmt: 数值, 超过此数值才会判定为true, 默认200
    fun: 函数, 接受拖动判定的中间值做自定义动作
return:
    无
中间结果(一个对象):
    w: 布尔, 是否向上, 不直接返回wasd字符串是因为可能有斜方向的需要
    s: 布尔, 是否乡下
    a: 布尔, 是否向左
    d: 布尔, 是否乡右
    horizontal: 布尔, 是否是水平的(只要水平拖动距离比竖直大就会true)
    length: int, (竖直或水平取最大)拖动的距离
    length2: int, 拖动距离(勾股定理计算得出)
使用方法:
    都是箭头函数, 一般不会出现this问题
    const f = this.$fun.mouseDrag
    const tdo = re => {
        console.log(re)
    }
    f(e, 200, tdo)
*/
const asyncMouseDrag = (e, lmt = 200) => {
	return new Promise(resolve => {
		if (e.type !== 'mousedown' || e.which !== 1) {
			return // 按下左键
		}
		const [x0, y0] = [e.pageX, e.pageY]
		const move = e => {
			const [x1, y1] = [e.pageX, e.pageY]
			const [lx, ly] = [Math.abs(x1 - x0), Math.abs(y1 - y0)]
			const re = {
				w: y0 - lmt > y1,
				s: y0 + lmt < y1,
				a: x0 - lmt > x1,
				d: x0 + lmt < x1,
				horizontal: lx > ly,
				length: Math.max(lx, ly),
				length2: Math.pow(lx * lx + ly * ly, 0.5) | 0,
			}
			resolve(re)
			document.removeEventListener('mouseup', move)
		}
		document.addEventListener('mouseup', move)
	})
}
export {asyncMouseDrag}
/*
mouseDrag的异步方法, 可以通过await直接得到re中间值
const f = this.$fun.asyncMouseDrag
const gragRe = await f(e, 200)
*/
