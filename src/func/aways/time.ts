function full(s: Number): string {
	return String(s).padStart(2, '0')
}
function Time(a: any) {
	const t: Date = a ? new Date(a.toString()) : new Date()
	const Y: string = full(t.getFullYear())
	const M: string = full(t.getMonth() + 1)
	const D: string = full(t.getDate())
	const h: string = full(t.getHours())
	const m: string = full(t.getMinutes())
	const s: string = full(t.getSeconds())
	const re: Object = {
		int: t.getTime(),
		Y,
		M,
		D,
		h,
		m,
		s,
		format(b0: string, b1: string): string {
			if (b0 === undefined) {
				const a: string = `${Y}年${M}月${D}日`
				const b: string = `${h}时${m}分${s}秒`
				return a + ' ' + b
			} else {
				b1 = b1 || b0
				const a = [Y, M, D]
				const b = [h, m, s]
				return a.join(b0) + ' ' + b.join(b1)
			}
		},
	}
}

export default Time
