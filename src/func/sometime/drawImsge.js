var canvas = document.getElementById('canvas')
var ctx = canvas.getContext('2d')
var inn = document.querySelector('table').innerHTML

var data =
	'<svg xmlns="http://www.w3.org/2000/svg" width="400" height="400">' +
	'<foreignObject width="100%" height="100%">' +
	'<div xmlns="http://www.w3.org/1999/xhtml" style="width:400px;height:400px;font-size:80px;display: flex;align-items: center;justify-content: center;">' +
	'<table style="margin:0 0">' +
	inn +
	'</table>' +
	'</div>' +
	'</foreignObject>' +
	'</svg>'

console.log(data)
var DOMURL = window.URL || window.webkitURL || window

var img = new Image()
var svg = new Blob([data], {type: 'image/svg+xml;charset=utf-8'})
var url = DOMURL.createObjectURL(svg)

img.onload = function() {
	ctx.drawImage(img, 0, 0)
	DOMURL.revokeObjectURL(url)
}

img.src = url

const iii = document.getElementById('iii')
iii.src = url
console.log(url)
