import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

const rt = new Router({
	mode: 'history',
	routes: [
		{
			path: '/home',
			alias: '/',
			name: 'home',
			component: () => import('./view/home/Index.vue'),
		},
	],
	scrollBehavior(to, from, savedPosition) {
		return {x: 0, y: 0}
	},
})
rt.beforeEach((to, from, next) => {
	// ...
	next()
})
export default rt
