import Vue from 'vue'
import App from './App.vue'
import router from './router' // vue-router
import store from './store/index' // vuex
import './func/aways/index' // 常用函数
import './registerServiceWorker' // pwa
import './style' // 全局样式

Vue.config.productionTip = false

new Vue({
	router,
	store,
	render: h => h(App),
}).$mount('#app')
