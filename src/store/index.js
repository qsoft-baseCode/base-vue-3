import Vue from 'vue'
import Vuex from 'vuex'
Vue.use(Vuex)

// 引入共享模块
import {test} from './test'
// 输出
export default new Vuex.Store({
	modules: {
		test,
	},
})
