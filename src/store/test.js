const test = {
	state: {
		some: 0,
	},
	mutations: {
		set_test_some(state, n) {
			state.some = n
		},
	},
}
export {test}
